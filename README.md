# Task Tracker Application

Test application:
1. Run docker container with test database:
> cd ./docker  
> docker-compose up
2. Start the spring boot application.
3. Use [swagger-ui](http://localhost:8080/task-tracker/swagger-ui.html) to send requests to application or any other tool.