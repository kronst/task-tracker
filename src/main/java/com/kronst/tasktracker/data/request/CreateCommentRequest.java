package com.kronst.tasktracker.data.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author KRonst
 */
@Data
public class CreateCommentRequest {
    @Schema(example = "1")
    private long authorId;
    @Schema(example = "2")
    private long taskId;
    @Schema(example = "WOW! That's nice!")
    private String text;
}
