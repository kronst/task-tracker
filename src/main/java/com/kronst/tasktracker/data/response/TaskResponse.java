package com.kronst.tasktracker.data.response;

import com.kronst.tasktracker.data.dto.AttachmentDto;
import com.kronst.tasktracker.data.dto.CommentDto;
import com.kronst.tasktracker.data.dto.DepartmentDto;
import com.kronst.tasktracker.data.dto.UserDto;
import com.kronst.tasktracker.enums.TaskStatus;
import java.time.LocalDate;
import java.util.List;
import lombok.Data;

/**
 * @author KRonst
 */
@Data
public class TaskResponse {
    private long id;
    private String name;
    private String description;
    private TaskStatus status;
    private DepartmentDto department;
    private UserDto author;
    private UserDto implementer;
    private List<CommentDto> comments;
    private List<AttachmentDto> attachments;
    private LocalDate createdAt;
    private LocalDate updatedAt;
}
