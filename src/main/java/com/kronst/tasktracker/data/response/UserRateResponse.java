package com.kronst.tasktracker.data.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author KRonst
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRateResponse {
    private long userId;
    private int rate;
}
