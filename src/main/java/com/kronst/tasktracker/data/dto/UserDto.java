package com.kronst.tasktracker.data.dto;

import com.kronst.tasktracker.storage.model.User;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author KRonst
 */
@Data
public class UserDto {
    @Schema(example = "1")
    private long id;
    @Schema(example = "John Doe")
    private String name;
    @Schema(example = "1234")
    private int rate;

    public static UserDto from(User user, int rate) {
        UserDto dto = new UserDto();
        dto.setId(user.getId());
        dto.setName(user.getName());
        dto.setRate(rate);
        return dto;
    }
}
