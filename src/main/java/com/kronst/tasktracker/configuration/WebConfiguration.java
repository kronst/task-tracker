package com.kronst.tasktracker.configuration;

import com.kronst.tasktracker.configuration.properties.UserRateProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author KRonst
 */
@Configuration
public class WebConfiguration {

    @Bean
    public RestTemplate userRateRestTemplate(
        RestTemplateBuilder builder,
        UserRateProperties properties
    ) {
        return builder.rootUri(properties.getBaseUrl())
            .setConnectTimeout(properties.getConnectionTimeout())
            .setReadTimeout(properties.getReadTimeout())
            .build();
    }
}
