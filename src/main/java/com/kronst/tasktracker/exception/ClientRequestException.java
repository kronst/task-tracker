package com.kronst.tasktracker.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * @author KRonst
 */
@Getter
public class ClientRequestException extends RuntimeException {
    private final HttpStatus status;
    private String description;

    public ClientRequestException(HttpStatus status) {
        super();
        this.status = status;
    }

    public ClientRequestException(HttpStatus status, String description) {
        super(description);
        this.status = status;
        this.description = description;
    }
}
