package com.kronst.tasktracker.exception.advice;

import com.kronst.tasktracker.exception.ClientRequestException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author KRonst
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ClientRequestException.class)
    public ResponseEntity<?> handleClientException(ClientRequestException ex) {
        return ResponseEntity
            .status(ex.getStatus())
            .body(ex.getDescription());
    }
}
