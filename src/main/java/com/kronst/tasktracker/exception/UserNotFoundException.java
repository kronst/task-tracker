package com.kronst.tasktracker.exception;

import org.springframework.http.HttpStatus;

/**
 * @author KRonst
 */
public class UserNotFoundException extends ClientRequestException {

    public UserNotFoundException(long id) {
        super(HttpStatus.NOT_FOUND, String.format("User with ID=%d not found", id));
    }
}
