package com.kronst.tasktracker.exception;

import org.springframework.http.HttpStatus;

/**
 * @author KRonst
 */
public class AttachmentNotFoundException extends ClientRequestException {

    public AttachmentNotFoundException(long attachmentId) {
        super(HttpStatus.NOT_FOUND, String.format("Attachment with ID=%d not found", attachmentId));
    }
}
