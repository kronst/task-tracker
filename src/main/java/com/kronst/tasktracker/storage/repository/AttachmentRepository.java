package com.kronst.tasktracker.storage.repository;

import com.kronst.tasktracker.storage.model.Attachment;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author KRonst
 */
public interface AttachmentRepository extends JpaRepository<Attachment, Long> {

    List<Attachment> findAllByTaskId(long taskId);
}
