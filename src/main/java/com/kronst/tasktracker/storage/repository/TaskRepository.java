package com.kronst.tasktracker.storage.repository;

import com.kronst.tasktracker.storage.model.Task;
import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author KRonst
 */
public interface TaskRepository extends JpaRepository<Task, Long> {

    List<Task> findAllByDepartmentId(long departmentId, Sort sort);
}
