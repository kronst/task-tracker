package com.kronst.tasktracker.storage.repository;

import com.kronst.tasktracker.storage.model.Comment;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author KRonst
 */
public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findAllByTaskId(long taskId);
}
