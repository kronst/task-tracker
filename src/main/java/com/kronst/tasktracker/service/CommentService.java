package com.kronst.tasktracker.service;

import com.kronst.tasktracker.data.request.CreateCommentRequest;
import com.kronst.tasktracker.exception.TaskNotFoundException;
import com.kronst.tasktracker.exception.UserNotFoundException;
import com.kronst.tasktracker.storage.model.Comment;
import com.kronst.tasktracker.storage.repository.CommentRepository;
import com.kronst.tasktracker.storage.repository.TaskRepository;
import com.kronst.tasktracker.storage.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author KRonst
 */
@Service
@RequiredArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final TaskRepository taskRepository;

    public long addComment(CreateCommentRequest request) {
        Comment comment = new Comment();
        comment.setAuthor(
            userRepository.findById(request.getAuthorId())
                .orElseThrow(() -> new UserNotFoundException(request.getAuthorId()))
        );
        comment.setTask(
            taskRepository.findById(request.getTaskId())
                .orElseThrow(() -> new TaskNotFoundException(request.getTaskId()))
        );
        comment.setText(request.getText());

        return commentRepository.saveAndFlush(comment).getId();
    }

    public void deleteComment(long id) {
        commentRepository.deleteById(id);
    }
}
