INSERT INTO tracker.departments (name, description, created_at, updated_at)
VALUES
       ('Tech Department', 'Detailed information about tech department', NOW(), NOW()),
       ('Finance Department', 'Detailed information about finance department', NOW(), NOW()),
       ('HR Department', 'Detailed information about HR department', NOW(), NOW());

INSERT INTO tracker.users (name, department_id, created_at, updated_at)
VALUES
       ('John Doe', 1, NOW(), NOW()),
       ('Jane Doe', 2, NOW(), NOW()),
       ('Mike Doe', 3, NOW(), NOW());
